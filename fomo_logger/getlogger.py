from colorama import Fore, Back, init, Style
from datetime import date, datetime
from logging import basicConfig, INFO, StreamHandler, getLogger
from os import listdir, makedirs, path, remove
from sys import stdout


class ColourStreamHandler(StreamHandler):
    """ A colorized output SteamHandler """

    # Some basic colour scheme defaults
    init()
    colours = {
        'DEBUG': Fore.CYAN,
        'INFO': Fore.GREEN,
        'WARN': Fore.YELLOW,
        'WARNING': Fore.YELLOW,
        'ERROR': Fore.RED,
        'CRIT': Back.RED + Fore.WHITE,
        'CRITICAL': Back.RED + Fore.WHITE
    }

    def emit(self, record):
        try:
            message = self.format(record)
            self.stream.write(
                self.colours[record.levelname] + message + Style.RESET_ALL)
            self.stream.write(getattr(self, 'terminator', '\n'))
            self.flush()
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)


def logFolderExist(log_path):
    """Returns True if logging folder exists."""

    return path.exists(log_path)


def createLogFolder(log_path):
    """Creates Log Foler"""

    makedirs(log_path)


def generateLogNameFromDateTime():
    """Generates the name of a log file from the current time."""

    return 'LOGFILE_({}).log'.format(date.today().isoformat().split(
        '.')[0].replace('T', '_'))


def getDateTimeObjectFromFileName(file):
    """Parses datetime object from a file name."""

    return datetime.strptime(file, 'LOGFILE_(%Y-%m-%d).log')


def deleteOldLogs(log_path):
    """Deletes Logs if there are more than 4."""
    while len(listdir(log_path)) > 4:
        logsFromFolder = listdir(log_path)
        datetimes = []
        for log in logsFromFolder:
            datetimes.append(getDateTimeObjectFromFileName(log))
        last_log = datetimes.index(min(datetimes))
        remove(path.join(log_path, logsFromFolder[last_log]))


def GetLogger(name, file):
    """Returns a logger object."""
    base_path = path.dirname(path.abspath(file))
    log_path = path.join(base_path, 'logs')

    if not logFolderExist(log_path):
        createLogFolder(log_path)

    if len(listdir(log_path)) > 4:
        deleteOldLogs(log_path)

    file_name = path.join(log_path, generateLogNameFromDateTime())
    basicConfig(filename=file_name, level=INFO,
                format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch = ColourStreamHandler(stdout)
    name = path.basename(name)
    logger = getLogger(name)
    logger.addHandler(ch)
    return logger


# if __name__ == '__main__':
#     LOGGER = GetLogger(__name__, __file__)
#     LOGGER.debug('testing debug...')
#     LOGGER.info('testing info...')
#     LOGGER.warning('testing warning...')
#     LOGGER.error('testing error...')
#     LOGGER.critical('testing critical...')
