from datetime import date
from os.path import exists, join

from fomo_logger import getlogger


def test_create_file(tmpdir):
    p = tmpdir.mkdir('log')


def test_logFolderExist(tmpdir):
    file = tmpdir.mkdir('log')
    assert getlogger.logFolderExist(file) == True


def test_createLogFolder(tmpdir):
    file = tmpdir.join('output.log')
    getlogger.createLogFolder(file)
    assert exists(file) == True


def test_generateLogNameFromDateTime():
    """Generates the name of a log file from the current time."""
    data = getlogger.generateLogNameFromDateTime()
    assert data == 'LOGFILE_({}).log'.format(
        date.today().isoformat().split('.')[0].replace('T', '_'))

# def test_getDateTimeObjectFromFileName(file):
    # """Parses datetime object from a file name."""
    # return datetime.strptime(file, 'LOGFILE_(%Y-%m-%d).log')
#
# def test_GetLogger(capsys, tmpdir):
#     """Returns a logger object."""
#     name = 'test_name'
#     file = tmpdir.mkdir('base')
#     log = getlogger.GetLogger(name, file)
#     log.info('kevin')
#     logfile = join(file, 'logs')
#     with open(logfile, 'r') as f:
#         pass
    # assert f.read() == 'kevin'
