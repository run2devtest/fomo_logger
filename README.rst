fomo_logger
===========

A package for logging fomo-dd applications.

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`fomo_logger` was written by `Kevin McGee <run2devtest@gmail.com>`_.
